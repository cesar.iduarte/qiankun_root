import './app.css';
import { registerMicroApps, start } from 'qiankun';
import ReactDOM from 'react-dom';
import App from './app';


ReactDOM.render(<App />, document.querySelector('#root'));

registerMicroApps([
  {
    name: 'react-app', // app name registered
    entry: 'http://localhost:7100/',
    container: '#viewport',
    activeRule: '/react-app',
  },
]);

start();