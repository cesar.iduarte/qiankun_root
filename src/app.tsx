const App = () => {
    return (
    <>
        <nav className="header">
            <ul className="header__wrapper">
                <li className="header__item"><a href="/react-app">App 1</a></li>
                <li className="header__item">App 2</li>
            </ul>
        </nav>
        <div id="viewport">
        </div>
    </>);
};

export default App;