# qiankun root sample app
Qiankun sample root app for loading microfront-ends.

## How to use it
#### Clone the repo
```
git clone git@gitlab.com:cesar.iduarte/qiankun_root.git
```
#### Install dependencies
```
npm i
```

#### Run it
Launches the app in dev mode (only mode tested so far)
```
npm run start
```

## Add MFEs
There is already one configured MFE which correspond to the https://gitlab.com/cesar.iduarte/qiankun_cra_sample repo. Adjust the index.tsx accordingly